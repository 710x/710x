<h1 align="center">Hi 👋, I'm 710</h1>
<h3 align="center">Vuong Sy Hanh</h3>

- 🦾 I'm currently a Backend developer at [Hamsa Corporation](https://hamsa.co/)

- 🔭 I’m currently studying at [Hanoi University of Industry](https://www.haui.edu.vn/en) in [Software Engineering](https://fit.haui.edu.vn/en)

- 📫 How to reach me **syhanhcbq@gmail.com**

<div align="center">

## 💻 My skills:

![My Skills](https://skillicons.dev/icons?i=nodejs,js,ts,docker,postman,git,mongo,express,nest,mysql,graphql,vscode,md,supabase,bots,gcp,postgres,prisma,kafka,nginx,redis)

</div>